Polypass
========
[![Gem Version](https://badge.fury.io/rb/polypass.svg)](https://badge.fury.io/rb/polypass) [![pipeline status](https://gitlab.com/ssofos/polypass/badges/master/pipeline.svg)](https://gitlab.com/ssofos/polypass/commits/master) [![coverage report](https://gitlab.com/ssofos/polypass/badges/master/coverage.svg)](https://gitlab.com/ssofos/polypass/commits/master)

Polypass is a polymorphous password generator written in the Ruby Programming Language and leveraging Ruby's SecureRandom module.
It can generate cryptographically secure random passwords including alphanumeric, alphanumeric symbol, and natural language.
Also, it can output the generated passwords in a variety of different data formats like JSON and YAML for ease of use and processing.

If you need to generate a useful on-demand secure and random password from the command-line,
or for programmatic consumption from within your Ruby application, Polypass can take care of it with ease.

## Install

To install polypass via RubyGems simply execute this command from a command-line interface:

```
gem install polypass
```

To use polypass as part of your Ruby Project add this line to your Gemfile:

```
gem 'polypass'
```

## Usage | CLI

After installing the Polypass RubyGem, the **polypass** command should be available to execute from a command-line interface.

By default Polypass generates 32 character length passwords.

Polypass uses the [Thor RubyGem](http://whatisthor.com/) for its CLI framework.

For help information on the `polypass create` sub command, execute this command from a command-line interface:

```
polypass help create
```

### Alphanumeric Passwords

To generate a secure random alphanumeric password execute this command from a command-line interface:

```
polypass create alphanum
```

To generate a secure random alphanumeric password greater than the default 32 characters, execute this command:

```
polypass create alphanum -l 64
```

To output password in JSON format, execute this command:

```
polypass create alphanum -o json
```

To output password in YAML format, execute this command:

```
polypass create alphanum -o yaml
```

### Alphanumeric Symbol Passwords

To generate a secure random alphanumeric symbol password execute this command from a command-line interface:

```
polypass create alphasym
```

To generate a secure random alphanumeric symbol password greater than the default 32 characters, execute this command:

```
polypass create alphasym -l 64
```

To output password in JSON format, execute this command:

```
polypass create alphasym -o json
```

To output password in YAML format, execute this command:

```
polypass create alphasym -o yaml
```

### Natural Language Passwords

To generate a secure random natural language password using the [literate_randomizer](https://rubygems.org/gems/literate_randomizer) sentence generator library, execute this command from a command-line interface:

```
polypass create natural
```

By default a secure random four(4) character alphanumeric string is appended to the end of the natural language password. To increase of size of the alphanumeric character string, execute this command:

```
polypass create natural -l 16
```

To output password in JSON format, execute this command:

```
polypass create natural -o json
```

To output password in YAML format, execute this command:

```
polypass create natural -o yaml
```

## Usage | Rubygem

One you have included Polypass in your Ruby Project's Gemfile and installed it as a RubyGem, to load it add this to your Ruby application:

```
require 'polypass'
```

### Classes

Polypass has these password generation Ruby Classes:

* `Polypass::AlphaNumeric`
  * Secure Random alphanumeric password generation
  * `.create` method return value Type: **String**
* `Polypass::AlphaSymbol`
  * Secure Random alphanumeric symbol password generation
  * `.create` method return value Type: **String**
* `Polypass::NaturalLanguage`
  * Secure Random natural language password generation
  * `.create` method return value Type: **String**

The Polypass Class and Sub-Classes have these common initialization arguments:

* length
  * The length of the password you want PolyPass to generate
  * Default: **32**
  * Type: **Integer**
* output_type
  * The output format of the password
  * Default: **stdout**
      * **'json'** for JSON output
      * **'yaml'** for YAML output
  * Type: **String**

### Instantiation

To create new secure random passwords simply invoke the `.new` method on any of the Polypass Sub-Classes.

Each Polypass Sub-Class has a common method called `.create` that will generate new secure random passwords for that Sub-Class.

### Alphanumeric Passwords

* Generate a default **32** character length secure random alphanumeric password:

```
Polypass::AlphaNumeric.new.create
```

* Output a different character length password:

```
Polypass::AlphaNumeric.new(64).create
```

* Output password in JSON format:

```
Polypass::AlphaNumeric.new(64, 'json').create
```

* Parse the JSON formatted password:

```
require 'json'

JSON.parse(Polypass::AlphaNumeric.new(64, 'json').create)
```

* Output password in YAML format:

```
Polypass::AlphaNumeric.new(64, 'yaml').create
```

* Load the YAML formatted password:

```
require 'yaml'

YAML.load(Polypass::AlphaNumeric.new(64, 'yaml').create)
```

### Alphanumeric Symbol Passwords

* Generate a default **32** character length secure random alphanumeric symbol password:

```
Polypass::AlphaSymbol.new.create
```

* Output a different character length password:

```
Polypass::AlphaSymbol.new(64).create
```

* Output password in JSON format:

```
Polypass::AlphaSymbol.new(64, 'json').create
```

* Parse the JSON formatted password:

```
require 'json'

JSON.parse(Polypass::AlphaSymbol.new(64, 'json').create)
```

* Output password in YAML format:

```
Polypass::AlphaSymbol.new(64, 'yaml').create
```

* Load the YAML formatted password:

```
require 'yaml'

YAML.load(Polypass::AlphaSymbol.new(64, 'yaml').create)
```

#### Natural Language Passwords

* **Note:** Natural language passwords are randomly generated from variable length [Markov chain](https://en.wikipedia.org/wiki/Markov_chain) derived sentences.
  * Instead of controlling the password character length, the length argument in the Polypass::NaturalLanguage Class controls a secure random alphanumeric string amended to the end of the natural language generated password.
  * This is inspired by the concept of a [cryptographic salt](https://en.wikipedia.org/wiki/Salt_(cryptography))
  * Default length for this pseudo-salt is **32**

* Generate a default length secure random natural language password:

```
Polypass::NaturalLanguage.new.create
```

* Output a password with different character length pseudo-salt:

```
Polypass::NaturalLanguage.new(16).create
```

* Output password in JSON format:

```
Polypass::NaturalLanguage.new(16, 'json').create
```

* Parse the JSON formatted password:

```
require 'json'

JSON.parse(Polypass::NaturalLanguage.new(16, 'json').create)
```

* Output password in YAML format:

```
Polypass::NaturalLanguage.new(16, 'yaml').create
```

* Load the YAML formatted password:

```
require 'yaml'

YAML.load(Polypass::NaturalLanguage.new(16, 'yaml').create)
```
