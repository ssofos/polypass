# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('lib', __dir__)
require 'polypass/version'

Gem::Specification.new do |s|
  s.name        = 'polypass'
  s.date        = '2018-07-12'
  s.version     = Polypass::VERSION
  s.authors     = ['Shane R. Sofos']
  s.email       = ['ssofos@gmail.com']
  s.homepage    = 'https://shanesofos.com/projects/polypass'
  s.license     = 'GPL-3.0'
  s.metadata    = { 'source_code_uri' => 'https://gitlab.com/ssofos/polypass' }
  s.description = 'The polymorphous password generator.'
  s.summary     =
    'A simple multitool that can generate different types of passwords ' +
    'including secure random, alphanumeric, natural language, and custom, ' +
    'salt, hash, and output them in different formats for personal and application consumption.'

  s.files         = %x(git ls-files).split($INPUT_RECORD_SEPARATOR)
  s.bindir        = ['bin']
  s.executables   = s.files.grep(%r{^bin/}) { |f| File.basename(f) }
  s.test_files    = s.files.grep(%r{^(test|spec|features)/})
  s.require_paths = ['lib']

  s.add_dependency 'literate_randomizer'
  s.add_dependency 'thor'
  s.add_development_dependency 'bundler'
  s.add_development_dependency 'rake'
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'rubocop', '<=0.56.0'
  s.add_development_dependency 'simplecov'

  s.required_ruby_version = '>= 2.4.0'
end
