# frozen_string_literal: true

require 'polypass'

describe 'Polypass Helpers' do
  before :all do
    @polypass = Polypass.new
  end

  describe '.alphanumeric_chars' do
    it 'returns 62 alphanumeric characters as an Array class object' do
      expect(@polypass.alphanumeric_chars).to be_kind_of(Array)
      expect(@polypass.alphanumeric_chars.length).to eq(62)
    end
  end

  describe '.alphasymbol_chars' do
    it 'returns 92 alphanumeric and symbol characters as an Array class object' do
      expect(@polypass.alphasymbol_chars).to be_kind_of(Array)
      expect(@polypass.alphasymbol_chars.length).to eq(92)
    end
  end

  describe '.output_json' do
    it 'should respond to 1 argument' do
      expect(@polypass).to respond_to(:output_json).with(1).argument
    end
  end

  describe '.output_yaml' do
    it 'should respond to 1 argument' do
      expect(@polypass).to respond_to(:output_yaml).with(1).argument
    end
  end

  describe '.random_element' do
    it 'should respond to 1 argument' do
      expect(@polypass).to respond_to(:random_element).with(1).argument
    end

    it 'returns a secure random single element from an Array as a String class object' do
      expect(@polypass.random_element(@polypass.alphanumeric_chars)).to be_kind_of(String)
      expect(@polypass.random_element(@polypass.alphanumeric_chars).length).to eq(1)
    end
  end

  describe '.random_number' do
    it 'should respond to 1 argument' do
      expect(@polypass).to respond_to(:random_element).with(1).argument
    end

    context 'default integer argument of 1000' do
      it 'returns a secure random number less than or equal to 1101 as an Integer class object' do
        expect(@polypass.random_number).to be_kind_of(Integer)
        expect(@polypass.random_number).to be <= 1101
      end
    end

    context 'integer argument of 10000' do
      it 'returns a secure random number less than or equal to 10101 as an Integer class object' do
        expect(@polypass.random_number).to be_kind_of(Integer)
        expect(@polypass.random_number).to be <= 10101
      end
    end
  end

  describe '.random_sample_chars' do
    it 'should respond to 2 arguments' do
      expect(@polypass).to respond_to(:random_sample_chars).with(2).arguments
    end

    context 'random alphanumberic characters with default length' do
      it 'returns a random sample of 32 characters as a String class object' do
        expect(@polypass.random_sample_chars(@polypass.alphanumeric_chars)).to be_kind_of(String)
        expect(@polypass.random_sample_chars(@polypass.alphanumeric_chars).length).to eq(32)
      end
    end

    context 'random alphanumberic and symbol characters with default character length' do
      it 'returns a random sample of 32 characters as a String class object' do
        expect(@polypass.random_sample_chars(@polypass.alphasymbol_chars)).to be_kind_of(String)
        expect(@polypass.random_sample_chars(@polypass.alphasymbol_chars).length).to eq(32)
      end
    end

    context 'random alphanumberic and symbol characters with 128 character length' do
      it 'returns a random sample of 32 characters as a String class object' do
        expect(@polypass.random_sample_chars(@polypass.alphasymbol_chars)).to be_kind_of(String)
        expect(@polypass.random_sample_chars(@polypass.alphasymbol_chars, 128).length).to eq(128)
      end
    end
  end

  describe '.random_sample_set' do
    it 'should respond to 3 arguments' do
      expect(@polypass).to respond_to(:random_sample_set).with(3).arguments
    end

    context 'given an alphanumeric character type with defult length of 32 and rounds of 1000' do
      it 'should return a set of of less than or equal to 1101 random passwords as an Array class object' do
        expect(@polypass.random_sample_set('alphanumeric')).to be_kind_of(Array)
        expect(@polypass.random_sample_set('alphanumeric').length).to be <= 1101
      end
    end

    context 'given an alphansymbol character type with defult length of 32 and rounds of 1000' do
      it 'should return a set of of less than or equal to 1101 random passwords as an Array class object' do
        expect(@polypass.random_sample_set('alphasymbol')).to be_kind_of(Array)
        expect(@polypass.random_sample_set('alphasymbol').length).to be <= 1101
      end
    end

    context 'given an invalid character type argument' do
      it 'should raise an ArgumentError expection' do
        expect { @polypass.random_sample_set('foobar') }.to raise_error(ArgumentError)
      end
    end
  end
end
