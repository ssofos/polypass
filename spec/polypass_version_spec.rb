# frozen_string_literal: true

require 'polypass/version'

describe 'Polypass::Version' do
  it 'returns the RubyGem version of Polypass as a string' do
    expect(Polypass::VERSION).to be_a_kind_of(String)
  end
end
