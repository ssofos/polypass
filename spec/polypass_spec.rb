# frozen_string_literal: true

require 'polypass'

describe Polypass do
  before :all do
    @polypass = Polypass.new
  end

  it 'is a Polypass object' do
    expect(@polypass).to be_kind_of(Polypass)
  end

  it 'should respond to 2 arguments when initialized' do
    expect(Polypass).to respond_to(:new).with(2).arguments
  end

  describe '#length' do
    it 'returns the length of the generated password as Integer class object' do
      expect(@polypass.length).to be_kind_of(Integer).and eq(32)
    end
  end

  describe '#output_type' do
    it 'returns the output type for generated passwords as a String class object' do
      expect(@polypass.output_type).to be_kind_of(String).and eq('stdout')
    end
  end
end
