# frozen_string_literal: true

require 'polypass'

describe Polypass::NaturalLanguage do
  before :all do
    @polypass_natural = Polypass::NaturalLanguage.new
  end

  it 'is a Polypass::NaturalLanguage object' do
    expect(@polypass_natural).to be_kind_of(Polypass::NaturalLanguage)
  end

  it 'should respond to 2 arguments when initialized' do
    expect(Polypass).to respond_to(:new).with(2).arguments
  end

  describe '#length' do
    it 'returns the length of the generated password as Integer class object' do
      expect(@polypass_natural.length).to be_kind_of(Integer).and eq(32)
    end
  end

  describe '#output_type' do
    it 'returns the output type for generated passwords as a String class object' do
      expect(@polypass_natural.output_type).to be_kind_of(String).and eq('stdout')
    end
  end

  describe '#language_data' do
    it 'returns paths of language data files as an Array' do
      expect(@polypass_natural.language_data).to be_kind_of(Array)
    end
  end

  describe '.init_language_randomizer' do
    it 'initializes and returns a LiterateRandomizer class object' do
      language_randomizer = @polypass_natural.init_language_randomizer
      expect(language_randomizer).to be_a_kind_of(LiterateRandomizer::Randomizer)
    end
  end

  describe '.create' do
    context 'stdout output type' do
      it 'returns a secure random natural language password as a String class object' do
        expect(@polypass_natural.create).to be_kind_of(String)
      end
    end

    context 'json output type' do
      it 'returns a secure random natural language password as a JSON formatted String class object' do
        expect(Polypass::NaturalLanguage.new(32, 'json').create).to be_kind_of(String)
      end
    end

    context 'yaml output type' do
      it 'returns a secure random natural language password as a YAML formatted String class object' do
        expect(Polypass::NaturalLanguage.new(32, 'yaml').create).to be_kind_of(String)
      end
    end
  end
end
