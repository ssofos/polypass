# frozen_string_literal: true

require 'polypass'

describe Polypass::AlphaSymbol do
  before :all do
    @polypass_alphasymbol = Polypass::AlphaSymbol.new
  end

  it 'is a Polypass::AlphaSymbol object' do
    expect(@polypass_alphasymbol).to be_kind_of(Polypass::AlphaSymbol)
  end

  it 'should respond to 2 arguments when initialized' do
    expect(Polypass).to respond_to(:new).with(2).arguments
  end

  describe '#length' do
    it 'returns the length of the generated password as Integer class object' do
      expect(@polypass_alphasymbol.length).to be_kind_of(Integer).and eq(32)
    end
  end

  describe '#output_type' do
    it 'returns the output type for generated passwords as a String class object' do
      expect(@polypass_alphasymbol.output_type).to be_kind_of(String).and eq('stdout')
    end
  end

  describe '.create' do
    context 'stdout output type' do
      it 'returns a secure random generated alphanumeric symbol 32 character password as a String class object' do
        expect(@polypass_alphasymbol.create).to be_kind_of(String)
        expect(@polypass_alphasymbol.create.length).to eq(32)
      end
    end

    context 'json output type' do
      it 'returns a secure random generated alphanumeric symbol password as JSON formatted String class object' do
        expect(Polypass::AlphaSymbol.new(32, 'json').create).to be_kind_of(String)
      end
    end

    context 'yaml output type' do
      it 'returns a secure random generated alphanumeric symbol password as YAML formatted String class object' do
        expect(Polypass::AlphaSymbol.new(32, 'yaml').create).to be_kind_of(String)
      end
    end
  end
end
