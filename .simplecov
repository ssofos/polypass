SimpleCov.start do
  add_group 'Libraries', 'lib'
  add_group 'RSpec Unit Tests', 'spec'
end
