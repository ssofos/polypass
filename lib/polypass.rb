# frozen_string_literal: true

require 'json'
require 'literate_randomizer'
require 'polypass/alpha_numeric'
require 'polypass/alpha_symbol'
require 'polypass/helpers'
require 'polypass/natural_language'
require 'polypass/version'
require 'securerandom'
require 'yaml'

# The polymorphous password generator
class Polypass
  attr_accessor :length, :output_type

  def initialize(length = 32, output_type = 'stdout')
    @length = length.to_i
    @output_type = output_type.to_s
  end

  # Set of general built-in helper methods
  include PolypassHelpers

  # Create and generate secure random alphanumeric passwords
  class AlphaNumeric < Polypass
    include PolypassAlphaNumeric
  end

  # Create and generate secure random alphanumeric symbol passwords
  class AlphaSymbol < Polypass
    include PolypassAlphaSymbol
  end

  # Create and generate secure random natural language passwords
  class NaturalLanguage < Polypass
    include PolypassNaturalLanguage
  end
end
