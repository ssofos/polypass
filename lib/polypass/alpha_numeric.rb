# frozen_string_literal: true

# Polypass::AlphaNumberic Class methods
module PolypassAlphaNumeric
  def initialize(length = 32, output_type = 'stdout')
    super(length, output_type)
  end

  def create
    case @output_type
    when 'stdout'
      random_element(random_sample_set('alphanumeric', @length))
    when 'json'
      output_json({ 'password' => random_element(random_sample_set('alphanumeric', @length)) })
    when 'yaml'
      output_yaml({ 'password' => random_element(random_sample_set('alphanumeric', @length)) })
    end
  end
end
