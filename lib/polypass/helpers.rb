# frozen_string_literal: true

# Collection of general Polypass helper methods
module PolypassHelpers
  def alphanumeric_chars
    characters = ('a'..'z').to_a + ('A'..'Z').to_a + ('0'..'9').to_a
    return characters
  end

  def alphasymbol_chars
    characters = ('a'..'z').to_a + ('A'..'Z').to_a + ('0'..'9').to_a
    characters += %w(! @ # $ % ^ & * \( \) { } [ ] - _ ~ < > ? ; : ` ~ / + = , . |)
    return characters
  end

  def output_json(hash)
    JSON.pretty_generate(hash)
  end

  def output_yaml(hash)
    YAML.dump(hash)
  end

  def random_element(array)
    array[SecureRandom.random_number(array.size)].to_s
  end

  def random_number(integer = 1000)
    (SecureRandom.random_number(integer) + (SecureRandom.random_number(100) + 1)).to_i
  end

  def random_sample_chars(chars, length = 32)
    (1..length).map { chars[SecureRandom.random_number(chars.size)] }.join
  end

  def random_sample_set(chartype, length = 32, rounds = 1000)
    random_chars = Array.new
    (1..random_number(rounds)).each do
      case chartype
      when 'alphanumeric'
        random_chars << random_sample_chars(alphanumeric_chars, length)
      when 'alphasymbol'
        random_chars << random_sample_chars(alphasymbol_chars, length)
      else
        raise ArgumentError,
              chartype + 'is an invalid chartype argument. alphanumeric and alphasymbol are the ' +
              'only valid chartype arguments for the random_sample_set method'
      end
    end
    return random_chars
  end
end
