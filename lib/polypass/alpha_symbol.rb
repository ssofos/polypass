# frozen_string_literal: true

# Polypass::AlphaSymbol Class methods
module PolypassAlphaSymbol
  def initialize(length = 32, output_type = 'stdout')
    super(length, output_type)
  end

  def create
    case @output_type
    when 'stdout'
      random_element(random_sample_set('alphasymbol', @length))
    when 'json'
      output_json({ 'password' => random_element(random_sample_set('alphasymbol', @length)) })
    when 'yaml'
      output_yaml({ 'password' => random_element(random_sample_set('alphasymbol', @length)) })
    end
  end
end
