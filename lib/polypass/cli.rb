# frozen_string_literal: true

require 'polypass'
require 'thor'

# Thor powered command-line interface
class PolypassCLI < Thor
  desc 'version', 'print the version of Polypass'
  def version
    puts 'polypass ' + Polypass::VERSION
  end

  desc 'create TYPE', 'create a TYPE secure random generated password'
  option :length, aliases: ['-l'], required: false, type: :numeric,
                  desc: 'length of password'
  option :output, aliases: ['-o'], required: false, type: :string,
                  desc: 'type of output formatter. Valid types are: stdout, json, yaml'
  long_desc <<-LONGDESC
    `polypass create TYPE` will generate a new secure random password.

    TYPE is the type of password Polypass will create such as alphanumeric,
    alphanumeric with symbols, natural language, and more.

    TYPE can be one of the following values:

    alphanum alphasym natural
  LONGDESC
  def create(type)
    length = options[:length].nil? ? 32 : options[:length]
    output = options[:output].nil? ? 'stdout' : options[:output]

    unless ['stdout', 'json', 'yaml'].include?(output)
      raise ArgumentError,
            output + ' is not a valid output type. ' +
            'Available types include: stdout, json, yaml'
    end

    case type
    when 'alphanum'
      puts 'Generating a new secure random alphanumeric password:'
      puts Polypass::AlphaNumeric.new(length, output).create
    when 'alphasym'
      puts 'Generating a new secure random alphanumeric symbol password:'
      puts Polypass::AlphaSymbol.new(length, output).create
    when 'natural'
      length = options[:length].nil? ? 4 : options[:length]
      puts 'Generating a new secure random natural language password:'
      puts Polypass::NaturalLanguage.new(length, output).create
    else
      raise ArgumentError,
            type + ' is not a valid password type. Avaiable types include: alphanum, alphasym, natural'
    end
  end
end
