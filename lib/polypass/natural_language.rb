# frozen_string_literal: true

# Polypass::NaturalLanguage Class methods
module PolypassNaturalLanguage
  attr_accessor :language_data

  def initialize(length = 32, output_type = 'stdout')
    super(length, output_type)
    @language_data = Dir.glob(File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'data')) + '/*.txt')
  end

  def init_language_randomizer
    return LiterateRandomizer.create(
      {
        source_material_file: random_element(@language_data),
        randomizer:  SecureRandom.random_number(Random.new_seed)
      }
    )
  end

  def create
    language = init_language_randomizer
    case @output_type
    when 'stdout'
      language.sentence + random_element(random_sample_set('alphanumeric', @length))
    when 'json'
      output_json({ 'password' => language.sentence + random_element(random_sample_set('alphanumeric', @length)) })
    when 'yaml'
      output_yaml({ 'password' => language.sentence + random_element(random_sample_set('alphanumeric', @length)) })
    end
  end
end
